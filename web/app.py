from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/")
def hello():
    #return "UOCIS docker demo!"
    return render_template('index.html')

@app.route('/filename')
def render():
    if 'filename' in request.args:
        myfilename = request.args.get('filename')
        if myfilename.find('..') == 0 or myfilename.find('~') == 0:
            return_template('403.html')
        return render_template(myfilename)
    else:
        #@app.errorhandler(404)
        #def error_404(404):
        return render_template('404.html')

if __name__ == "__main__":
    app.run(debug = True,host='0.0.0.0')
